var token;

$(function() {
    // Handler for .ready() called.

    token = localStorage.getItem('jwt-token');
    console.log(token);

    if (!token) {
        window.location.href = 'login.html';
    } else {
        console.log('Token is '+token);
    }

    $.ajax
    ({
        type: "GET",
        url: 'https://fusio-api.test/iabapp/authors',
        contentType: "application/json",
        dataType: 'json',
        async: false,
    }).done(function(data){
        var template = $('#row-template').html();

        for(var x in data.entry ) {

            var new_row = template.replace("{id}", data.entry[x].id);
            new_row = new_row.replace('{name}', data.entry[x].name);
            $('tbody').append( new_row );
        }
    }).fail(function(data){
        alert('Invalid username or password');
    });

});