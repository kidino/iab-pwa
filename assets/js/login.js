$(function() {

    $('#login-button').on('click', function(){
        $.ajax
        ({
            type: "POST",
            url: 'https://fusio-api.test/consumer/login',
            contentType: "application/json",
            dataType: 'json',
            async: false,
            data: JSON.stringify( {
                username : $('#username').val(),
                password : $('#password').val(),
            } ),
        }).done(function(data){
            console.log(data);
            localStorage.setItem('jwt-token', data.token);
            window.location.href = '/';

        }).fail(function(data){
            alert('Invalid username or password');
        });     
    });

});