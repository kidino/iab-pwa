var cacheName = 'kelasp-cache';
var filesToCache = [
  '/',
  '/index.html',
  '/login.html',
  '/assets/css/styles.css',
  '/assets/css/Login-Form-Clean.css',
  '/assets/bootstrap/css/bootstrap.min.css',
  '/assets/bootstrap/js/bootstrap.min.js',
  '/assets/js/main.js',
  '/assets/js/login.js',
  '/assets/js/app.js',
  '/assets/js/jquery.js',
  '/assets/fonts/fa-brands-400.eot',
  '/assets/fonts/fa-brands-400.svg',
  '/assets/fonts/fa-brands-400.ttf',
  '/assets/fonts/fa-brands-400.woff',
  '/assets/fonts/fa-brands-400.woff2',
  '/assets/fonts/fa-regular-400.eot',
  '/assets/fonts/fa-regular-400.svg',
  '/assets/fonts/fa-regular-400.ttf',
  '/assets/fonts/fa-regular-400.woff',
  '/assets/fonts/fa-regular-400.woff2',
  '/assets/fonts/fa-solid-900.eot',
  '/assets/fonts/fa-solid-900.svg',
  '/assets/fonts/fa-solid-900.ttf',
  '/assets/fonts/fa-solid-900.woff',
  '/assets/fonts/fa-solid-900.woff2',
  '/assets/fonts/fontawesome-all.min.css',
  '/assets/fonts/ionicons.eot',
  '/assets/fonts/ionicons.min.css',
  '/assets/fonts/ionicons.svg',
  '/assets/fonts/ionicons.ttf',
  '/assets/fonts/ionicons.woff',
  '/assets/img/kp-icon-192.png',
  '/assets/img/kp-icon-512.png',
  '/assets/img/kp-icon-128.png',
  '/assets/img/kp-icon-256.png',
  '/assets/img/kp-icon-144.png',
];

/* Start the service worker and cache all of the app's content */
self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});

/* Serve cached content when offline */
self.addEventListener('fetch', function(e) {
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});